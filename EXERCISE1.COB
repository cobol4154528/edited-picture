       IDENTIFICATION DIVISION. 
       PROGRAM-ID. Ex1.
       
       DATA DIVISION. 
       WORKING-STORAGE SECTION.
       01  NUM1-1 PIC 9(5) VALUE 12345. 
       01  NUM1-2 PIC 9(5) VALUE 01234. 
       01  NUM1-3 PIC 9(5) VALUE 00123. 
       01  NUM1-4 PIC 9(5) VALUE 00012. 
       01  EDIT-EX1 PIC *****.
       PROCEDURE DIVISION.
      
           MOVE NUM1-1 TO EDIT-EX1
           DISPLAY "Ex1 Data 1 : " EDIT-EX1  
           MOVE NUM1-2 TO EDIT-EX1  
           DISPLAY "Ex1  Data 2 : " EDIT-EX1 
           MOVE NUM1-3 TO EDIT-EX1  
           DISPLAY "Ex1  Data 3 : " EDIT-EX1
           MOVE NUM1-4 TO EDIT-EX1  
           DISPLAY "Ex1 DATA 4 : " EDIT-EX1
           DISPLAY "***************************"

           
          .
